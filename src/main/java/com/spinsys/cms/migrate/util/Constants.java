package com.spinsys.cms.migrate.util;


public interface Constants
{
	public static enum DataFormat {
		FORM( "application/x-www-form-urlencoded" ),
		JSON( "application/json" ),
		XML( "application/xml" );
		
		public String getMimeType() { return mimeType; }
		
		private DataFormat( String mimeType ) {
			this.mimeType = mimeType;
		}
		
		private final String mimeType;
	};
	
	public static enum DocumentType {
		CASE(
				// ??
				"/arkcase/api/latest/plugin/casefile",
				// caseFiles
				"/arkcase/latest/plugin/search/caseFilesSearch/byTimeInterval",
				// findCasesByTitle
				"/arkcase/latest/plugin/casefile/byTitle/@searchName@",
				// findCaseById
				"/arkcase/api/latest/plugin/casefile/byId/@queryId@" ),
		COMPLAINT(
				"/arkcase/api/latest/plugin/complaint",
				"/arkcase/api/latest/plugin/search/complaintsSearch/byTimeInterval",
				"/arkcase/api/latest/complaint/forUser/@searchName@",
				"/arkcase/api/latest/complaint/byId/@queryId@" ),
		COSTSHEET( "", "", "", "" ),
		LOGIN( "/arkcase/j_spring_security_check", null, null, null ),
		ORGANIZATION(
				"/arkcase/api/latest/plugin/organizations",
				"/arkcase/api/latest/plugin/organizations",
				"/arkcase/api/latest/plugin/organizations/search/@searchName@",
				"/arkcase/api/latest/plugin/organizations/@queryId@" ),
		PERSON(
				"/arkcase/api/latest/plugin/people",
				"/arkcase/api/latest/plugin/people",
				"/arkcase/api/latest/plugin/person/find?assocId=@searchName@",
				"/arkcase/api/latest/plugin/people/@queryId@" ),
		REPOSITORY( "", "", "", "" ),
		TASK( "", "", "", "" ),
		TIMESHEET( "", "", "", "" );
		
		public String getUpsertDestination()
		{
			return upsert;
		}
		public String getListAllDestination()
		{
			return listing;
		}
		public String getSearchDestination( String name )
		{
			return search.replaceAll( "@searchName@", name );
		}
		public String getQueryDestination( String id ) {
			return query.replaceAll( "@queryId@", id );
		}
		
		private DocumentType( String upsertDest, String listAllDest, String searchDest, String queryDest )
		{
			upsert = upsertDest;
			listing = listAllDest;
			search = searchDest;
			query = queryDest;
		}
		
		private final String upsert, listing, search, query;
	};

	public final static String HTTP_STATUS_CODE = "HTTP-Status";
	public final static String HTTP_FAULT_COUNT = "HTTP-Fault-Count";
	
	public final static String HTTP_ACCEPT_HDR = "Accept";
	public final static String HTTP_AUTHORIZATION_HDR = "Authorization";
	public final static String HTTP_CONTENT_LENGTH_HDR = "Content-Length";
	public final static String HTTP_CONTENT_TYPE_HDR = "Content-Type";
	public final static String HTTP_COOKIE_HDR = "Cookie";
	public final static String HTTP_LOCATION_HDR = "Location";
	public final static String HTTP_SET_COOKIE_HDR = "Set-Cookie";

	public final static String HTTP_USERNAME_PARAM = "j_username";
	public final static String HTTP_PASSWORD_PARAM = "j_password";
}
