/*
 * ArkCaseClient
 * 
 * Sep 9, 2021
 * 
 * Change Activity Log
 * Reason  Date     Who  Description
 * ------  -------- ---  -----------
 */
package com.spinsys.cms.migrate;

import com.spinsys.cms.migrate.util.Constants;
import com.veetechis.lib.io.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URI;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import javax.net.ssl.SSLContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;


/**
 * ArkCaseClient is an ArkCase REST data access and migration tool that... more
 * should be written about.
 */
public class ArkCaseClient
implements Constants
{
	/**
	 * Returns the client instance for the specified server URL.
	 * 
	 * @param url  the ArkCase server URL (e.g. https://arkcase-ce.local).
	 * @return  the client instance.
	 * @throws java.net.MalformedURLException
	 */
	public static synchronized ArkCaseClient getInstance( String url )
	throws MalformedURLException
	{
		ArkCaseClient instance = instanceMap.get( url );
		if (instance == null) {
			instance = new ArkCaseClient( url );
			instanceMap.put( url, instance );
		}
		
		return instance;
	}
	

	/**
	 * Attempts to authenticate with the ArkCase server using the ArkCase REST
	 * API service layer. Returns a client session identifier if authentication
	 * is successful.
	 * 
	 * @param username  the user's login identifier.
	 * @param password  the user's login password.
	 * @return  the authenticated client session identifier, or null.
	 * @throws Exception
	 */
	public String authenticate( String username, String password )
	throws Exception
	{
		String sessionId = null;
		
		HashMap<String,String> requestHeaders = new HashMap<>();
		requestHeaders.put( HTTP_CONTENT_TYPE_HDR, DataFormat.FORM.getMimeType() );
		
		HashMap<String,String> responseHeaders = new HashMap<>();
		String destination = String.format( "%s?%s=%s&%s=%s", new Object[] {DocumentType.LOGIN.getUpsertDestination(),
			HTTP_USERNAME_PARAM, username, HTTP_PASSWORD_PARAM, password} );
		callService( sessionId, destination, requestHeaders, "", responseHeaders );
		
		// ArkCase returns a new session ID in a cookie for every authentication
		// request, good or bad, but also usually responds with a "redirect"
		// (302) to one of the following locations:
		//   Success -> /arkcase/home.html#!/welcome
		//   Error   -> /arkcase/login?login_error
		//
		// Just check for a non-Error redirect to indicate success and grab the
		// session ID to return to the caller for subsequent identification.
		//
		if (responseHeaders.containsKey(HTTP_STATUS_CODE)) {
			int status = Integer.parseInt( responseHeaders.get(HTTP_STATUS_CODE) );
			if ((status < 400 && status >= 200)) {
				if (responseHeaders.containsKey(HTTP_LOCATION_HDR)) {
					String location = responseHeaders.get( HTTP_LOCATION_HDR );
					if (!location.toLowerCase().contains("login_error")) {	// success!(?)
						if (responseHeaders.containsKey(HTTP_SET_COOKIE_HDR)) {
							String cookie = responseHeaders.get( HTTP_SET_COOKIE_HDR ).trim();
							if (cookie.startsWith("JSESSIONID")) {
								sessionId = cookie.split( "\\;" )[0].split( "\\=" )[1];
								sessionsMap.put( sessionId, cookie );
							}
						}
					}
				}
			}
		}
		
		return sessionId;
	}
	
	/**
	 * Retrieves the list of specified document entries using the ArkCase REST
	 * API service layer and returns them in the specified format.
	 * 
	 * @param sessionId  the client session identifier.
	 * @param docType  the document set to retrieve.
	 * @param dataFormat  the data format to return.
	 * @return 
	 * @throws Exception
	 */
	public String getDocuments( String sessionId, DocumentType docType, DataFormat dataFormat )
	throws Exception
	{
		HashMap<String,String> requestHeaders = new HashMap<>();
		requestHeaders.put( HTTP_ACCEPT_HDR, dataFormat.getMimeType() );
			
		HashMap<String,String> responseHeaders = new HashMap<>();
		String responseBody = callService( sessionId, docType.getListAllDestination(), requestHeaders, null, responseHeaders, true );

		// TODO: convert response if needed...
		return responseBody;
	}

	/**
	 * Retrieves the list of specified document entries matching the given
	 * criteria using the ArkCase REST API service layer and returns them in the
	 * specified format. If doQuery is true then the given criteria is used to
	 * to query a specific document by ID, otherwise it is used to search for
	 * documents with matching information.
	 * 
	 * @param sessionId  the client session identifier.
	 * @param docType  the document set to retrieve.
	 * @param dataFormat  the data format to return.
	 * @param criteria  the document lookup criteria (e.g. ID or matching name).
	 * @param doQuery  perform a specific document search by ID if true.
	 * @return 
	 * @throws Exception
	 */
	public String getDocuments( String sessionId, DocumentType docType, DataFormat dataFormat, String criteria, boolean doQuery )
	throws Exception
	{
		HashMap<String,String> requestHeaders = new HashMap<>();
		requestHeaders.put( HTTP_ACCEPT_HDR, dataFormat.getMimeType() );
			
		HashMap<String,String> responseHeaders = new HashMap<>();
		String destination = (doQuery ? docType.getQueryDestination(criteria) : docType.getSearchDestination(criteria));
		String responseBody = callService( sessionId, destination, requestHeaders, null, responseHeaders, true );

		// TODO: convert response if needed...
		return responseBody;
	}

	/**
	 * Migrates data files in the named upload directory to ArkCase using the
	 * ArkCase REST API service layer.
	 * 
	 * @param sessionId
	 * @param fileName
	 * @param dataFormat
	 * @param docType
	 * @return 
	 * @throws Exception
	 */
	public String upsertData( String sessionId, String fileName, DataFormat dataFormat, DocumentType docType )
	throws Exception
	{
		String responseBody = "";
		
		File inputFile = new File( fileName );
		if (FileUtils.isReadableFile(inputFile)) {
			String requestBody;
			try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
				FileInputStream fis = new FileInputStream( inputFile );
				int b;
				while ((b = fis.read()) != -1) {
					baos.write( b );
				}
				baos.flush();
				requestBody = baos.toString();
			}
			
			HashMap<String,String> requestHeaders = new HashMap<>();
			requestHeaders.put( HTTP_ACCEPT_HDR, dataFormat.getMimeType() );
			requestHeaders.put( HTTP_CONTENT_TYPE_HDR, dataFormat.getMimeType() );
			
			HashMap<String,String> responseHeaders = new HashMap<>();
			responseBody = callService( sessionId, docType.getUpsertDestination(), requestHeaders, requestBody, responseHeaders );
		}
		
		return responseBody;
	}


	/**
	 * Sends the given request body to the service endpoint specified by the
	 * request path.The service is called using HTTP POST.
	 * 
	 * @param sessionId
	 * @param requestPath
	 * @param requestHeaders
	 * @param requestBody
	 * @param responseHeaders
	 * @return
	 * @throws Exception 
	 */
	protected String callService( String sessionId, String requestPath, HashMap<String,String> requestHeaders, String requestBody, HashMap<String,String> responseHeaders )
	throws Exception
	{
		return callService( sessionId, requestPath, requestHeaders, requestBody, responseHeaders, false );
	}

	/**
	 * Calls the service endpoint specified by the request path, optionally
	 * sending the given request body to it.If a query is indicated then the
	 * service is called using HTTP GET and the request body is ignored,
	 * otherwise the request body is sent to the service using HTTP POST.
	 * 
	 * @param sessionId
	 * @param requestPath
	 * @param requestHeaders
	 * @param requestBody
	 * @param responseHeaders
	 * @param isQuery
	 * @return
	 * @throws Exception 
	 */
	protected String callService( String sessionId, String requestPath,
			HashMap<String,String> requestHeaders, String requestBody,
			HashMap<String,String> responseHeaders, boolean isQuery )
	throws Exception
	{
		String response = "";
		
		int statusCode;
		try {
			HttpRequestBase request = createHttpRequest( requestPath, getHttpParameters(), requestBody, isQuery );
			if (sessionId != null) {
				requestHeaders.put( HTTP_COOKIE_HDR, sessionsMap.get(sessionId) );
			}
			requestHeaders.keySet().forEach(header -> {
				request.setHeader( header, requestHeaders.get(header) );
			});
			
			if( LOG.isInfoEnabled() ) {
				Header[] headers = request.getAllHeaders();
				StringBuilder buff = new StringBuilder( "[" );
				for( int i = 0; i < headers.length; i++ ) {
					if( i > 0 ) buff.append( ", " );
					buff.append( headers[i].getName() ).append( ": " ).append( headers[i].getValue() );
				}
				buff.append( "]" );
				String requestLine = request.getRequestLine().toString();
				if (DataFormat.FORM.getMimeType().equals(requestHeaders.get(HTTP_CONTENT_TYPE_HDR))) {
					requestLine = String.format( "%s?**parameters-masked**...", requestLine.split( "\\?" )[0] );
				}
				LOG.info( String.format("Sending request to ArkCase:\nRequest line = %s\nRequest headers = %s\nRequest entity = %s",
						new Object[] {requestLine, buff.toString(), requestBody}) );
			}
			
			long startTime = System.currentTimeMillis();
			HttpResponse resp = getHttpClient().execute( request );
			
			if( LOG.isTraceEnabled() ) {
				LOG.info( String.format("Elapsed time to call ArkCase: %s", String.valueOf(System.currentTimeMillis() - startTime)) );
			}
			
			statusCode = resp.getStatusLine().getStatusCode();
			responseHeaders.put( HTTP_STATUS_CODE, String.valueOf(statusCode) );
			
			if( LOG.isDebugEnabled() ) {
				LOG.debug( String.format("Received status code: %s for endpoint %s.", statusCode, requestPath) );
			}
			
			HttpEntity entity = resp.getEntity();
			if( entity != null ) {
				response = EntityUtils.toString( entity );
			}

			if( LOG.isInfoEnabled() ) {
				LOG.info( String.format("Received response from ArkCase:\n%s", response) );
			}
			
			for (Header header : resp.getAllHeaders()) {
				responseHeaders.put( header.getName(), header.getValue() );
			}
		}
		catch (IOException e) {
			if( LOG.isErrorEnabled() ) {
				LOG.error( "IO exception thrown calling service.", e );
			}
			if( (e instanceof ConnectException) || (e instanceof SocketException) ) {
//				heartbeatMonitor.setServerAlive( false );
			}
			responseHeaders.put( HTTP_FAULT_COUNT, String.valueOf(1) );
//			responseHeaders.put( HTTP_ONLINE_FLAG, String.valueOf(heartbeatMonitor.isServerAlive()) );
			
			// null out response, indicates the request wasn't successful
			response = "";
		}
		catch (Exception e) {
			if( LOG.isErrorEnabled() ) {
				LOG.error( "Unexpected exception thrown processing request.", e );
			}
			responseHeaders.put( HTTP_FAULT_COUNT, String.valueOf(1) );
//			responseHeaders.put( HTTP_ONLINE_FLAG, String.valueOf(heartbeatMonitor.isServerAlive()) );
			
			// null out response, indicates the request wasn't successful
			response = "";
		}
		
		return response;
	}

	protected HttpRequestBase createHttpRequest( String destination, HttpParams parameters, String body, boolean isQuery )
	throws Exception
	{
		HttpRequestBase request;
		if (isQuery) {
			request = new HttpGet();
		}
		else {
			request = new HttpPost();
			((HttpPost) request).setEntity( new StringEntity(body) );
		}
		request.setParams( parameters );
		request.setURI( new URI(url + destination) );

		return request;
	}
	
	protected CloseableHttpClient getHttpClient()
	{
		if (httpClient == null) {
			TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
			SSLContext sslContext;
			try {
				sslContext = SSLContexts.custom().loadTrustMaterial( null, acceptingTrustStrategy ).build();
				SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory( sslContext );

				Registry<ConnectionSocketFactory> socketFactoryRegistry =
						RegistryBuilder.<ConnectionSocketFactory> create()
								.register( "https", sslsf )
								.register( "http", new PlainConnectionSocketFactory() )
								.build();

				BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager( socketFactoryRegistry );
				httpClient = HttpClients.custom().
						disableCookieManagement().
						setSSLSocketFactory( sslsf ).
						setConnectionManager( connectionManager ).
						build();
			}
			catch ( NoSuchAlgorithmException | KeyStoreException | KeyManagementException e ) {
				if (LOG.isErrorEnabled()) {
					LOG.error( "Error creating HTTP client.", e );
				}
			}
		}
		
		return httpClient;
	}
	
	protected HttpParams getHttpParameters()
	{
		BasicHttpParams params = new BasicHttpParams();
		params.setIntParameter( CoreConnectionPNames.SO_TIMEOUT, 30000 );
		params.setIntParameter( CoreConnectionPNames.CONNECTION_TIMEOUT, 10000 );
		
		return params;
	}
	

	/*
	 * Constructor.
	 */
	private ArkCaseClient( String host )
	throws MalformedURLException
	{
		while (host.endsWith("/")) {
			host = host.substring( 0, host.length()-1 );
		}

		url = new URL( host );
		sessionsMap = new HashMap<>();
	}


	private CloseableHttpClient httpClient;
	
	private final HashMap<String,String> sessionsMap;
	private final URL url;

	private static final HashMap<String,ArkCaseClient> instanceMap = new HashMap<>();
	private static final Log LOG = LogFactory.getLog( ArkCaseClient.class );
}
