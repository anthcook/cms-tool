package com.spinsys.cms.migrate;

import com.spinsys.cms.migrate.util.Constants;
import com.veetechis.lib.io.FileUtils;
import com.veetechis.lib.preferences.ArgumentsParser;
import com.veetechis.lib.util.KeyValueList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class MigrationTool
{
	public static void main( String[] args )
	{
		try {
			Parameters parms = new Parameters( args );
			if (!parms.hasCredentials() || !parms.hasRequiredParameters()) {
				System.out.println( parms.getUsage() );
			}
			else {
				ArkCaseClient client = ArkCaseClient.getInstance( parms.hostUrl );
				String sessionId = client.authenticate( parms.username, parms.password );
				if (sessionId != null) {
					String response;
					if (parms.upsertFile != null) {
						response = client.upsertData( sessionId, parms.upsertFile, parms.dataFormat, parms.documentType );
					}
					else if (parms.criteria != null) {
						response = client.getDocuments( sessionId, parms.documentType, parms.dataFormat, parms.criteria, parms.criteriaIsQuery );
					}
					else {
						response = client.getDocuments( sessionId, parms.documentType, parms.dataFormat );
					}
					if (parms.responseFile != null && response != null) {
						File outFile = writeResponse( response, parms );
						if (outFile == null) {
							System.out.println( "Failed to write response file. See logs for error." );
						}
						else {
							System.out.println( String.format("Response written to file %s", outFile) );
						}
					}
				}
				else {
					System.out.println( String.format("Authentication with server '%s' failed.", parms.hostUrl) );
				}
			}
		}
		catch (Exception e) {
			if (LOG.isErrorEnabled()) {
				LOG.error( "Error running client.", e );
			}
		}
	}
	
	/*
	 * Writes the give response to one of the following:
	 * 1. The output file path specified in parameters if not a directory; or
	 *
	 * 2. The output directory specified in parameters with file name same as
	 * the input file but with '.rsp' suffix (e.g. --file=myinput.json ->
	 * myinput.rsp); or
	 *
	 * 3. The output directory specified in parameters with file name same as
	 * the specified document type with suffix '.rsp' if input file not
	 * specified.
	 */
	private static File writeResponse( String response, Parameters parms )
	{
		File outFile;
		if (parms.responseFile.isDirectory()) {
			if (parms.upsertFile != null) {
				int idx = parms.upsertFile.indexOf( "." );
				if (idx < 0) idx = parms.upsertFile.length();
				outFile = new File( parms.responseFile, parms.upsertFile.substring(0, idx) + ".rsp" );
			}
			else {
				outFile = new File( parms.responseFile, parms.documentType.name().toLowerCase() + ".rsp" );
			}
		}
		else {
			outFile = parms.responseFile;
		}
		
		try (FileOutputStream out = new FileOutputStream(outFile)) {
			out.write( response.getBytes() );
			out.flush();
		}
		catch( Exception e ) {
			outFile = null;
			if (LOG.isErrorEnabled()) {
				LOG.error( String.format("Error creating response file %s.", outFile) );
			}
		}
		
		return outFile;
	}

	
	static class Parameters
	{
		Parameters( String[] args )
		throws Exception
		{
			if (args.length > 0) {
				String outputPath = null;
				KeyValueList parms = new ArgumentsParser( ArgumentsParser.DEFAULT_SWITCH, "=" ).parse( args );
				if (parms.get("--parameters") != null) {
					File pfile = new File( parms.get("--parameters").getValue() );
					try {
						if (FileUtils.isReadableFile(pfile)) {
							Properties config = new Properties();
							config.load( new FileInputStream(pfile) );
							username = config.getProperty( "username" );
							password = config.getProperty( "password" );
							hostUrl = config.getProperty( "url" );
							criteria = config.getProperty( "query" );
							if (criteria != null) {
								criteriaIsQuery = true;
							}
							else {
								criteria = config.getProperty( "search" );
							}
							upsertFile = config.getProperty( "file" );
							outputPath = config.getProperty( "output" );
							if (config.getProperty("format") != null) {
								dataFormat = Constants.DataFormat.valueOf( config.getProperty("format").toUpperCase() );
							}
							if (config.getProperty("type") != null) {
								documentType = Constants.DocumentType.valueOf( config.getProperty("type").toUpperCase() );
							}
						}
						else {
							if (LOG.isWarnEnabled()) {
								LOG.warn( String.format("Parameters file '%s' not readable, input ignored.", pfile) );
							}
						}
					}
					catch (FileNotFoundException e) {
						if (LOG.isWarnEnabled()) {
							LOG.warn( String.format("Parameters file '%s' not found, input ignored.", pfile) );
						}
					}
				}
				if (parms.get("--username") != null) {
					username = parms.get( "--username" ).getValue();
				}
				if (parms.get("--password") != null) {
					password = parms.get( "--password" ).getValue();
				}
				if (parms.get("--url") != null) {
					hostUrl = parms.get( "--url" ).getValue().toLowerCase();
				}
				if (parms.get("--query") != null) {
					criteria = parms.get( "--query" ).getValue();
					criteriaIsQuery = true;
				}
				if (parms.get("--search") != null) {
					criteria = parms.get( "--search" ).getValue();
				}
				if (parms.get("--file") != null) {
					upsertFile = parms.get( "--file" ).getValue();
				}
				if (parms.get("--output") != null) {
					outputPath = parms.get( "--output" ).getValue();
				}
				if (parms.get("--format") != null) {
					dataFormat = Constants.DataFormat.valueOf( parms.get("--format").getValue().toUpperCase() );
				}
				if (parms.get("--type") != null) {
					documentType = Constants.DocumentType.valueOf( parms.get("--type").getValue().toUpperCase() );
				}
				
				if (outputPath != null) {
					responseFile = new File( outputPath );
				}
			}
		}

		boolean hasCredentials()
		{
			return (username != null && password != null);
		}
		
		boolean hasRequiredParameters()
		{
			return (hostUrl != null && dataFormat != null && documentType != null);
		}

		String getUsage()
		{
			return "Usage parameters:\n" +
					"\t--parameters=<properties file>\n" +
					"\t--username=<login ID>\n" +
					"\t--password=<credentials>\n" +
					"\t--url=<server url [e.g. https://arkcase-ce.local]>\n" +
					"\t--query=<identifier> **\n" +
					"\t--search=<identifier> **\n" +
					"\t--file=<file to input> **\n" +
					"\t--output=<path to write response> **\n" +
					"\t--format=[json|xml]\n" +
					"\t--type=[case|complaint|organization|person|task]\n\n" +
					"** If a file name is provided then a data upload is presumed,\n" +
					"   otherwise a document query is performed.\n\n" +
					"   An output path can be a named file or a directory. If a\n" +
					"   directory then the file name used is either the input file\n" +
					"   name or the document type, with suffix .rsp.\n\n" +
					"   If a numeric search or query ID is provided then only\n" +
					"   documents matching the criteria are returned from a document\n" +
					"   query. Otherwise, all documents of the specified type are\n" +
					"   returned.";
		}
		
		String hostUrl;
		String upsertFile;
		String username;
		String password;
		String criteria;
		File responseFile;
		boolean criteriaIsQuery;
		Constants.DataFormat dataFormat;
		Constants.DocumentType documentType;
	}

	
	/*
	static {
		String path = MigrationTool.class.getClassLoader().
				getResource( "logging.properties" ).getFile();
		System.setProperty( "java.util.logging.config.file", path );
		System.setProperty( "org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger" );
	}
	*/
	
	private static final Log LOG = LogFactory.getLog( MigrationTool.class );
}
