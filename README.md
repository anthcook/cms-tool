# README #

### What is this repository for? ###

A Java API for adding, updating ("upserting"), listing, and querying documents
in ArkCase through it's REST service layer, and CLI interface to test and
demonstrate usage. The tool accepts a JSON or XML formatted file for one of the
following ArkCase document types:

* Case - an investigation or workflow based on a complaint. (*TODO*)
* Complaint - a request and information to open and pursue a case. (*INCOMPLETE*)
* Organization - an organized group or entity involved or related to a complaint
or case.
* Person - an individual involved or related to a complaint or case. (*INCOMPLETE*)
* Task - a job or activity performed on a case. (*TODO*)

### How do I use it? (CLI) ###

* Clone this repository to your local workstation, change directory to
./cms-tool and build it with `mvn clean package`. Copy the resulting JAR file
in .../target to a suitable work location and run the jar without arguments to
see usage parameters, e.g.:
####
```
$ java -jar cms.migrate-0.0.1-SNAPSHOT.jar
Usage parameters:
        --parameters=<properties file>
        --username=<login ID>
        --password=<credentials>
        --url=<server url [e.g. https://arkcase-ce.local]>
        --query=<identifier> **
        --search=<identifier> **
        --file=<file to input> **
        --output=<path to write response> **
        --format=[json|xml]
        --type=[case|complaint|organization|person|task]

** If a file name is provided then a data upload is presumed,
   otherwise a document query is performed.

   An output path can be a named file or a directory. If a
   directory then the file name used is either the input file
   name or the document type, with suffix .rsp.

   If a numeric search or query ID is provided then only
   documents matching the criteria are returned from a document
   query. Otherwise, all documents of the specified type are
   returned.
```

* Create a properties file for frequently used parameters (e.g. username,
password, url, format), and use --parameters to specify their values without
having to explicitly pass them on the CLI, e.g.:

*myparms.cfg*
```
username=myid@arkcase.org
password=mypassw0rd
url=https://arkcase-ce.local
format=json
```

`$ java -jar cms.migrate-0.0.1-SNAPSHOT.jar --parameters=myparms.cfg
--type=organization # get list of organizations`

### How do I develop with it? (API) ###
(*TODO*)

See _com.spinsys.cms.migrate.MigrationTool_ for coding example.
